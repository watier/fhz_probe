import time
import serial
#import threading


    

class fht6020(object):
    def __init__(self, port, baudrate=19200, probeNumber=1, fhtAdress=1):
        self.port = port
        self.baudrate=baudrate
        self.probeNumber = probeNumber
        self.fhtAdress = fhtAdress
        self._connect()
        
    def _connect(self):    
        self.ser = serial.Serial(
            port=self.port,
            parity=serial.PARITY_EVEN,
            bytesize=serial.SEVENBITS,
            stopbits=serial.STOPBITS_TWO,
            baudrate=self.baudrate,
            timeout=2
            )        

    def _checkSum(self,command):
        sum=7  # As the first character sent will be ASCII=7
        for c in command:
            sum+=ord(c)    
        return hex(sum%256)[2:]


    def sendCmd(self, command):
        print("Sending "+command)
        toBeSend = chr(7)+command+self._checkSum(command)+chr(3)
        self.ser.write(toBeSend.encode())
        return self.ser.readline()


    @property
    def measuredValue(self):
        res = None
        try :
            res = self.sendCmd("{:02d}RM{:01d}".format(self.fhtAdress , self.probeNumber))
            res = float(res.rsplit()[1])
        except Exception as e:
            print("Error reading measured value: "+str(e))
            res = None
        return res

    @property
    def historyStoreInterval(self):
        res = self.sendCmd("01YR")
        res = res.rsplit()[0]
        return int(res[5:], 16)


    @historyStoreInterval.setter
    def historyStoreInterval(self, value):
        if value < 60 or value >= 3600:
            raise(ValueError("History interval must be within 60 and 3599"))
        self.sendCmd("01YW "+str(value))


    @property
    def systemStatus(self):
        return self.sendCmd("01##")[5:9]

    @property
    def alarmLevel(self):
        return int(self.sendCmd("01lR01").split()[1])
    
    @alarmLevel.setter
    def alarmLevel(self, value):
        self.sendCmd("01lW01 "+str(int(value)))


        
if __name__ == "__main__" :    
    sonde = fht6020("/dev/ttyUSB2")
    print(sonde.sendCmd("01RM1"))


    test = 0
    while True:
        test +=1
        print(str(test)+ "   Measured :"+str(sonde.measuredValue))
    
    # for i in range(3598, 3700):
    #     print(str(i))
    #     try: 
    #         sonde.historyStoreInterval = i
    #     except ValueError as e:
    #         print(str(e))
